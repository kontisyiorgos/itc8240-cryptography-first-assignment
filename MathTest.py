alphabet={}
alphabet_bin={}
alphabet_list=[]
alphabet_list_bin=[]
plaintext=[]
plaintext_value=[]
shifted_value=[]
shifted_text=[]
ciphertext=''
ciphertext_value_lst=[]
shift_key=0
permutation_key_str=''
permutation_key_lst=[]
vigenre_key_str=''
vigenre_key_lst=[]
OTP_cipher_str=''

def prepare_alphabet_dictionary():
    #prepare the alpahbet dictionary to be used
    alpha_char = "abcdefghijklmnopqrstuvwxyz"
    #alpha_char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    i=0
    for char in alpha_char:
        alphabet[char]=i
        alphabet_list.append(char)
        i+=1
    for ch in alphabet_list:
        binary=bin(ord(ch)-1)[4:]
        alphabet_bin[ch]=binary
        alphabet_list_bin.append(binary)
    return

prepare_alphabet_dictionary()

def zero_variables():
    global alphabet
    global alphabet_list
    global plaintext
    global plaintext_value
    global shifted_value
    global shifted_text
    global ciphertext
    global ciphertext_value_lst
    global shift_key
    global permutation_key_str
    global permutation_key_lst
    global vigenre_key_str
    global vigenre_key_lst
    global OTP_cipher_str
    alphabet={}
    alphabet_list=[]
    plaintext=[]
    plaintext_value=[]
    shifted_value=[]
    shifted_text=[]
    ciphertext=''
    ciphertext_value_lst=[]
    shift_key=0
    prepare_alphabet_dictionary()
    permutation_key_str=''
    permutation_key_lst=[]
    vigenre_key_str=''
    vigenre_key_lst=[]
    OTP_cipher_str=''
    return

def load_list_plaintext(plain_t):
    plain_t=plain_t.lower()
    for char in plain_t:
        plaintext.append(char)
        plaintext_value.append(alphabet[char])
    return 

def load_list_shifted_value_of_plaintext():
    cypher=''
    for i in range(len(plaintext_value)):
        shifted_value.append((plaintext_value[i] + shift_key) % 26)
    for j in shifted_value:
        shifted_text.append(alphabet_list[int(j)])
    for k in shifted_text:
        cypher += k
    return cypher

def calculate_permutation_lst():
    permutation_key_lst=permutation_key_str.split(",")
    for i in range(len(permutation_key_lst)):
        permutation_key_lst[i]=int(permutation_key_lst[i])
    return permutation_key_lst

def check_lengths_plaintext_pemutation_key():
    if (len(plaintext)%len(permutation_key_lst))!=0:
        print("The plaintext is not an exact multiple of the permutation key")
    return

def permutate_the_plaintext():
    key=len(permutation_key_lst)
    count=len(plaintext)/key
    start=[]
    remaining=[]
    result=[]
    i=0
    j=0
    ciphertext=''
    while count>0:
        j=j+key
        start=plaintext[i:j]
        remaining=plaintext[j:]
        for k in permutation_key_lst:
            result.append(start[k-1])
        i=i+key
        count-=1
    for z in result:
        ciphertext+=z
    return ciphertext

def encrypt_with_vigenere():
    for char in vigenre_key_str:
        vigenre_key_lst.append(alphabet[char])
    for i in range(len(plaintext_value)):
        position=plaintext_value[i]
        key_position=vigenre_key_lst[i%len(vigenre_key_lst)]
        value=position+key_position
        value=value%26
        ciphertext_value_lst.append(value)
    cypher=''
    for k in ciphertext_value_lst:
        cypher += alphabet_list[k]
    return cypher

def find_index_of_coincidence(text):
    text=text.lower()
    result=dict()
    for ch in text:
        if ch not in result:
            result[ch]=1
        else:
            result[ch]=result[ch]+1
    N=len(text)
    N=N*(N-1)
    Sigma=0
    for key in result:
        SigmaNi=0
        Ni=result[key]
        Ni=Ni*(Ni-1)
        SigmaNi=Ni/N
        Sigma=Sigma+SigmaNi
    return Sigma

def xnor(ch1,ch2):
    temp=int(ch1) ^ int(ch2)
    if temp == 1:
        return '0'
    elif temp == 0:
        return '1'
    else:
        print("xor error")

def decrypt_OTP(plain_t,cipher_t):
    plain_t=plain_t.lower()

    plaintext_binary=''
    for ch in plain_t:
        plaintext_binary=plaintext_binary+alphabet_bin[ch]
    ciphertext_binary=cipher_t

    if len(ciphertext_binary)!=len(plaintext_binary):
        print("Error. Cipher and plain text do not have the same length")
        return
    
    result=''
    for i in range(len(ciphertext_binary)):
        result=result+(xnor(ciphertext_binary[i],plaintext_binary[i]))    

    i=0
    k=len(plain_t)
    encryption_key=''
    while k<=len(ciphertext_binary):
        for key, value in alphabet_bin.items():
            if value == ciphertext_binary[i:k]:
                encryption_key=encryption_key+key
        i=i+len(plain_t)
        k=k+len(plain_t)
    print(f"The OTP key in binary was \t{result}")
    return encryption_key.upper()

def encrypt_w_OTP(plain_t,cipher_t):
    plain_t=plain_t.lower()
    
    ciphertext_binary=cipher_t
    plaintext_binary=''
    
    for ch in plain_t:
        plaintext_binary=plaintext_binary+alphabet_bin[ch]
    
    if len(ciphertext_binary)!=len(plaintext_binary):
        print("Error. Cipher and plain text do not have the same length")
        return
    
    result=''
    for i in range(len(ciphertext_binary)):
        result=result+str((int(ciphertext_binary[i])^int(plaintext_binary[i])))    

    i=0
    k=len(plain_t)
    encryption_key=''
    while k<=len(ciphertext_binary):
        for key, value in alphabet_bin.items():
            if value == ciphertext_binary[i:k]:
                encryption_key=encryption_key+key
        i=i+len(plain_t)
        k=k+len(plain_t)
    print(f"The OTP key in binary was \t{result}")
    print(f"The OTP key in text was \t{encryption_key}")
    return



def encrypt_with_affine(plain_t):
    t_coef_a=int(input("\tEnter coeficient A: "))
    t_coef_b=int(input("\tEnter coeficient B: "))
    t=0
    ciphertext=''
    for i in plaintext_value:
        t=(i * t_coef_a) + t_coef_b
        t=t%26
        ciphertext_value_lst.append(t)
        for key,value in alphabet.items():
            if value==t:
                ciphertext=ciphertext+key
    return ciphertext
  

#---------------------------------------------

while True:
    print("-"*80)
    print(alphabet)
    print()
    print("Choices:")
    print("\tCalculate text value:\t1")
    print("\tStart w Shift   :\t2\tContinue w Shift   :\t3")
    print("\tStart w Permut  :\t4\tContinue w Permut  :\t5")
    print("\tStart w Vigenere:\t6\tContinue w Vigenere:\t7")
    print("\tIndex of coincidence:\t8\tFind OTP\t   :\t9")
    print("\tText to 5-bit binary:\t10\tFind inverse mod 26 :\t11")
    print("\tEncrypt w Affine:\t12\tFind GCD mod 26 :\t13")
    print("\tEncrypt w OTP   :\t14")
    my_input=input("Enter number: ")

    if int(my_input) == 1:
        zero_variables()
        load_list_plaintext(input("\tEnter plain text: "))
        print(f"\tThe value of the plaintext is: {plaintext_value}")
    elif int(my_input) == 2:
        zero_variables()
        load_list_plaintext(input("\tEnter plain text: "))
        shift_key=int(input("\tEnter the shift key: "))
        ciphertext=load_list_shifted_value_of_plaintext()
        print(f"\tThe value of the plaintext is: {plaintext_value} and the value of the cipher is {shifted_value}")
        print(f"\tThe ciphertext is: {ciphertext}")
    elif int(my_input) == 3:
        previous_cypher=ciphertext
        zero_variables()
        load_list_plaintext(previous_cypher)
        shift_key=int(input("\tEnter the NEW shift key: "))
        ciphertext=load_list_shifted_value_of_plaintext()
        print(f"\tThe NEW value of the plaintext is: {plaintext_value} and the value of the cipher is {shifted_value}")
        print(f"\tThe NEW ciphertext is: {ciphertext}")
    elif int(my_input) == 4:
        zero_variables()
        load_list_plaintext(input("\tEnter plain text: "))
        permutation_key_str=input("\tEnter the permutation key separated by comma: ")
        permutation_key_lst=calculate_permutation_lst()
        check_lengths_plaintext_pemutation_key()
        ciphertext=permutate_the_plaintext()
        print(f"\tThe ciphertext is: {ciphertext}")
    elif int(my_input) == 5:
        previous_cypher=ciphertext
        zero_variables()
        load_list_plaintext(previous_cypher)
        permutation_key_str=input("\tEnter the NEW permutation key separated by comma: ")
        permutation_key_lst=calculate_permutation_lst()
        check_lengths_plaintext_pemutation_key()
        ciphertext=permutate_the_plaintext()
        print(f"\tThe NEW ciphertext is: {ciphertext}")
    elif int(my_input) == 6:
        zero_variables()
        load_list_plaintext(input("\tEnter plain text: "))
        vigenre_key_str=input("\tEnter the Vigenere key: ").lower()
        ciphertext=encrypt_with_vigenere()
        print(f"\tThe ciphertext is: {ciphertext}")        
    elif int(my_input) == 7:
        previous_cypher=ciphertext
        zero_variables()
        load_list_plaintext(previous_cypher)
        vigenre_key_str=input("\tEnter the NEW Vigenere key: ").lower()
        ciphertext=encrypt_with_vigenere()
        print(f"\tThe NEW ciphertext is: {ciphertext}")        
    elif int(my_input) == 8:
        zero_variables()
        ciphertext=find_index_of_coincidence(input("\tEnter text: ").lower())
        print(f"\tThe IC is: {ciphertext}")        
    elif int(my_input) == 9:
        zero_variables()
        t_plaintext=input("\tEnter plain text: ").lower()
        OTP_cipher_str=input("\tEnter the ciphertest: ").lower()
        print(f"The OTP key in text was \t{decrypt_OTP(t_plaintext,OTP_cipher_str)}")
    elif int(my_input) == 10:
        zero_variables()
        t_text=input("\tEnter text: ").lower()
        t_result=''
        for ch in t_text:
            t_result=t_result+alphabet_bin[ch]
        print(f"The binary representation of the text is {t_result}")
    elif int(my_input) == 11:
        zero_variables()
        num=int(input("\tEnter number to search: "))
        k=0
        for i in range(1000):
            t=(i*num)%26
            if t==1:
                print(i,f"\tnumber {num} has as inverse the number {i}")
                k+=1
                if k>5:
                    break
    elif int(my_input) == 12:
        zero_variables()
        load_list_plaintext(input("\tEnter plain text: "))
        ciphertext=encrypt_with_affine(plaintext)
        print(f"The cipher text is {ciphertext} with a value of {ciphertext_value_lst}")
    elif int(my_input) == 13:
        zero_variables()
        import math
        print("\tIn the range [-100:0] the following numbers are coprime with 26:")
        for i in range(-100,0):
            if math.gcd(i,26)==1:
                print(f"\t{i}", end =" ")
        print("\n")
        print("\tIn the range [0:100] the following numbers are coprime with 26:")
        for i in range(100):
            if math.gcd(i,26)==1:
                print(f"\t{i}", end =" ")
        print("\n")
    elif int(my_input) == 14:
        zero_variables()
        t_plaintext=input("\tEnter plain text: ").lower()
        OTP_key_str=input("\tEnter the OTP key in binary: ").lower()
        encrypt_w_OTP(t_plaintext,OTP_key_str)
    else:
        print("Wrong choice, try again5")

